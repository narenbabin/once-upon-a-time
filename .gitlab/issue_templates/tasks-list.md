<!-- please add a title to your issue -->

Hey 👋 <!-- @handle -->

It's time to launch our amazing landing page 🚀

We need to be facilitate the day to day job of the team, so can you 🙏 please add these "enhancements" to the project?

### 🔎 Testing, Quality and Security

- we need testing
- we need to check the quality of the source code
- we must pay attention to the vulnerabilities (bad code habits, secrets, ...)


### 🚀 Deployment

We need to simplify web application deployments:

- add docker image build automation
- automate the deployment of the application in production
- add the ability to review the application before deploying it

<!-- 🖐️ don't forget to assign the issue to somebody -->
/assign @k33g
/label ~todo ~enhancement 


